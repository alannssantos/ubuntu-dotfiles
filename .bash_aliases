#### Variaveis.

RCor="\[\033[1;31m\]"    # Red (Vermelho)
GCor="\[\033[1;32m\]"    # Green (Verde)
YCor="\[\033[1;33m\]"    # Yellow (Amarelo)
BCor="\[\033[1;34m\]"    # Blue (Azul)
PCor="\[\033[1;35m\]"    # Purple (Roxo)
CCor="\[\033[1;36m\]"    # Cyan (Turquesa)
WCor="\[\033[1;37m\]"    # White (Branco)
ECor="\[\033[0m\]"       # End (Fim)


#### Começo da Funções git status. ####

#### Pega git branch da Pasta.
function parse_git_branch() {
	BRANCH=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=$(parse_git_dirty)
		echo -e " [${BRANCH}]"
	else
		echo ""
	fi
}

#### Pega git status da Pasta.
function parse_git_dirty {
	status=$(git status 2>&1 | tee)
    untracked=$(echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?")
    ahead=$(echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?")
    dirty=$(echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?")
	MNumber=$(echo "${status}" | grep -c "modified:")
    newfile=$(echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?")
	NNumber=$(echo "${status}" | grep -c "new file:")
    renamed=$(echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?")
	RNumber=$(echo "${status}" | grep -c "renamed:")
    deleted=$(echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?")
    DNumber=$(echo "${status}" | grep -c "deleted:")
    bits=''
	if [ "${renamed}" == "0" ]; then
		bits="$RNumber${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="$NNumber${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="${DNumber}${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="$MNumber${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo -e " ${bits}"
	else
		echo ""
	fi
}

#### Fim da Função git status ####
export PS1="\\n$RCor[$WCor\A$RCor] $GCor\u$YCor@$BCor\h $GCor\w$ECor\\n$YCor\$(parse_git_branch)$ECor$CCor\$(parse_git_dirty)$ECor $PCor$ $ECor"

#### Aliases
alias ceni='sudo /usr/sbin/Ceni'
alias legenda='subliminal download -l pt-BR'
alias youtubemusic-dl='youtube-dl --extract-audio --audio-format mp3 -l --embed-thumbnail --add-metadata'
alias kcc-coltmod='kcc-c2e --profile=K578 --upscale --splitter=2'

#### Exports
export EDITOR="vim"
export PATH="$PATH:~/.config/scripts"

### Man Page Colors
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

### Mostrar arquivos ocultos com FZF e abri com o aplcativo padrão pra determinado arquivo.

export FZF_DEFAULT_COMMAND='find /home /media'

selecter() {
rifle "$(fzf -e | xargs -r -0)"
}

finder() {
ranger --selectfile="$(fzf -e | xargs -r -0)"
}

bind '"\C-F":"finder\n"'
bind '"\C-X":"selecter\n"'
bind '"\C-A":"mounty\n"'

ufetch
