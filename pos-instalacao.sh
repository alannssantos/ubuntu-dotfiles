#!/usr/bin/env bash

#     _    _
#    / \  | | __ _ _ __  _ __      Alann Santos
#   / _ \ | |/ _` | '_ \| '_ \     https://gitlab.com/alannssantos
#  / ___ \| | (_| | | | | | | |
# /_/   \_\_|\__,_|_| |_|_| |_|

## Cores.
RCor="\033[1;31m"    # Red (Vermelho)
GCor="\033[1;32m"    # Green (Verde)
YCor="\033[1;33m"    # Yellow (Amarelo)
BCor="\033[1;34m"    # Blue (Azul)
WCor="\033[1;37m"    # White (Branco)
ECor="\033[0m"       # End (Fim)

## Mensagens

Sussesso="$YCor
╭────────────────────────╮
│$GCor Instalado com sucesso!$YCor │
╰────────────────────────╯
$ECor"
Fracasso="$YCor
╭────────────────────────╮
│  $RCor Erro na instalação!$YCor  │
╰────────────────────────╯
$ECor"

Menu(){
echo -e "$BCor
	1.Base
	2.Sistema
	3.Temas
	4.Bat e Google Chrome
	5.Fzf e Vundle.vim
	6.Nerd Fonts e Ranger config $YCor(pode demorar bastante tempo.)$BCor
	7.Qtile
	8.XFCE
	9.Config
$ECor"
read -r Resposta

case "$Resposta" in
	1)
		#### Instalador de PPA's.
		sudo apt install software-properties-common \
			ubuntu-restricted-extras -y && \
			echo -e "$BCor\n01.Base!$ECor"
			echo -e "$Sussesso" || \
			echo -e "$Fracasso"
		sudo add-apt-repository ppa:xuzhen666/dockbarx -y && \
		echo -e "$GCor\nRepositorio DockBarX Instalado!$ECor\n" || \
		echo -e "$RCor\nErro na Insatação do Repositorio DockBarX!$ECor\n"
		sudo add-apt-repository ppa:system76/pop -y && \
		echo -e "$GCor\nRepositorio (Pop! os) Instalado!$ECor\n" || \
		echo -e "$RCor\nErro na Insatação do Repositorio (Pop! os)!$ECor\n"
		sudo add-apt-repository ppa:lutris-team/lutris -y && \
		echo -e "$GCor\nRepositorio Lutris Instalado!$ECor\n" || \
		echo -e "$RCor\nErro na Insatação do Repositorio Lutris!$ECor\n"
		Menu ;;

	2)
		#### Aplicativos e Drivers Pos-instalação.
		sudo apt install gparted \
			rxvt \
			curl \
			lynx \
			xorg \
			feh \
			redshift \
			lightdm \
			lightdm-gtk-greeter-settings \
			ranger \
			pcmanfm \
			file-roller \
			rofi \
			aptitude \
			alsa-utils \
			mpv \
			compton \
			atool \
			python3-pip \
			mpg123 \
			firefox \
			vim \
			gnome-calculator \
			zathura \
			rar \
			system-config-printer \
			cups* \
			xfce4-notifyd \
			ffmpegthumbnailer \
			w3m-img \
			gedit \
			neomutt \
			qbittorrent \
			streamlink \
			irssi \
			peek \
			youtube-dl \
			mps-youtube \
			network-manager \
			simple-scan \
			blueman \
			wine-stable \
			winetricks \
			software-properties-gtk \
			lutris -y && \
#
		sudo apt update && \
		sudo apt upgrade -y && \
		sudo -H pip3 install youtube-dl --upgrade && \
		sudo -H pip3 install subliminal --upgrade && \
		echo -e "$BCor\n02.Sistema!$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;

	3)
		sudo apt install papirus-icon-theme \
			arc-theme \
			breeze-cursor-theme \
			pop-fonts \
			pop-icon-theme \
			pop-gtk-theme -y && \
		sudo apt update && \
		sudo apt upgrade -y && \
		echo -e "$BCor\n03.Temas do Sistema!$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;

	4)
		#### Bat (cat mais bonito.)
		url1="https://github.com"
		url2=$(wget -qO- "https://github.com/sharkdp/bat/releases" \
			| grep "<a href=" \
			| grep "bat_" \
			| grep "_amd64.deb" \
			| sed '1!d' \
			| sed 's/^.*href="//' \
			| sed 's/" rel=.*$//')
		wget -c "$url1$url2" && \
		sudo dpkg -i bat_*_amd64.deb && \
		echo -e "$BCor\n04.Instalacao do Bat!$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"

		#### Google Chrome.
		wget -c https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
		sudo dpkg -i google-chrome-stable_current_amd64.deb && \
		sudo apt install -f && \
		echo -e "$BCor\n04.Google Chrome.$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;

	5)
		#### Instalação do FZF e Vundle.vim
		git clone --depth 1 https://github.com/junegunn/fzf.git ~/.config/fzf && ~/.config/fzf/install --all && \
		git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim && \
		echo -e "$BCor\n05.Instalacao do Fuzzy Finder e Vundle.vim$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;

	6)
		#### Nerd Fonts e Ranger config.
		git clone https://github.com/ryanoasis/nerd-fonts.git && cd nerd-fonts && ./install.sh && \
		git clone https://github.com/alexanderjeurissen/ranger_devicons ~/.config/ranger/plugins/ranger_devicons && cd ~/.config/ranger/plugins/ranger_devicons && make install && \
		echo -e "$BCor\n06.Nerd Fonts e Ranger config.$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;

	7)
		sudo apt install python3-pip \
			gnome-screenshot \
			qtile \
			lxappearance -y && \
		sudo -H pip3 install xcffib --upgrade && \
		sudo -H pip3 install cairocffi --upgrade && \
		sudo -H pip3 install qtile==0.13.0 --upgrade && \
		echo -e "$BCor\n07.Instalacao do Qtile.$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;
	8)
		sudo apt install xfwm4 \
			xfce4-datetime-plugin \
			xfce4-genmon-plugin \
			xfce4-notifyd \
			xfce4-panel \
			xfce4-power-manager \
			xfce4-pulseaudio-plugin \
			xfce4-session \
			xfce4-settings \
			xfce4-taskmanager \
			xfce4-whiskermenu-plugin \
			xfdashboard \
			dockbarx \
			xfce4-dockbarx-plugin \
			update-notifier \
			pavucontrol \
			network-manager \
			wicd-cli \
			wicd-curses \
			wicd-gtk \
			smplayer \
			viewnior \
			xdotool \
			evince \
			gdebi -y && \
#
		sudo apt update && \
		sudo apt upgrade -y &&
		echo -e "$BCor\n08.Instalacao do XFCE4$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
        Menu;;
	9)
		#### Copiar Dotfiles.
		cp -r -v .config .irssi .vim .bash_aliases .nanorc .tmux.conf .vimrc .Xresources ~/ && \
		echo -e "$BCor\n09.Copiando arquivos de configuração...$ECor" && \
		echo -e "$Sussesso" || \
		echo -e "$Fracasso"
		Menu ;;
	q)
		exit ;;
	*)
		clear
		echo -e "$YCor

		╭────────────────────────╮
		│$RCor   Opção desconhecida.$YCor  │
		╰────────────────────────╯
			$ECor"
		Menu ;;
esac
}
Menu
