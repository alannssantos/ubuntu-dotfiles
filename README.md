# My Qtile Dotfiles, Thanks DT.

![Qtile/Ubuntu Logo](https://gitlab.com/alannssantos/ubuntu-dotfiles/raw/master/.screenshots/qtile-ubuntu.png "Logo Qtile/Ubuntu")

![Screenshot of my desktop](https://gitlab.com/alannssantos/ubuntu-dotfiles/raw/master/.screenshots/dotfiles1.png "Screenshot")

#### A full-featured, pure-Python tiling window manager
---
# Atalhos Qtile
Atalhos | Funções
------------ | -------------
`Alt+Tab` | Abre Rofi ( Menu de Programas )
`Mod+Enter` | Abre Rxvt ( Terminal )
`Mod+w` | Fecha janela
`Mod+(←,→)` | Muda o foco da janela atual
`Mod+(a,s,d,f,u,i,o,p)` | Muda Area de Trabalho
`Mod+Shift+(a,s,d,f,u,i,o,p)` | Muda a janela selecionada para a Area de Trabalho desejada
`Mod+(1..9)` | Abre alguns programas
`Mod+Tab` | Muda o Layout da Area de Trabalho atual
`Mod+Control+r` | Reinicia o Qtile
`Mod+Control+q` | Sai do Qtile
---
### Upgrading Qtile
`$ sudo -H pip3 install xcffib --upgrade`

`$ sudo -H pip3 install cairocffi --upgrade`

`$ sudo -H pip3 install qtile==0.13.0 --upgrade`

# Dependencias

* Feh (Wallpaper)
* LightDM GTK+ Greeter (Gerenciador de Login)
* Rxvt (Terminal Padrão) 
* Ranger (Gerenciador de Arquivos Padrão)
* Pcmanfm (Gerenciador de Arquivos Secundario)
* Aptitude (Updates)
* Alsamixer (Controle de Audio)
* Mpv (Reprodutor de Video)
* Google-chrome (Navegador Padrão)
* Vim (Editor de Texto)
* Gnome-Screenshot (Capturar Tela)
* Lxappearance (Gerenciador de Temas)

# PPA
```
$ sudo apt install software-properties-common
$ sudo apt install ubuntu-restricted-extras
$ sudo add-apt-repository ppa:system76/pop
```
# Configuração

#### Ativar o Network Manager (nmtui).

```
$ service network-manager restart
```

#### Crontab `/etc/crontab`.

```
# For details see man 4 crontabs  
  
# Example of job definition:  
# .---------------- minute (0 - 59)  
# | .------------- hour (0 - 23)  
# | | .---------- day of month (1 - 31)  
# | | | .------- month (1 - 12) OR jan,feb,mar,apr ...  
# | | | | .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat  
# | | | | |  
# * * * * * user-name command to be executed
*/30  *     *    * *    root    /usr/bin/apt update >/dev/null 2>&1
*/30  */3   */3  * *    alann   /home/alann/.config/scripts/wallpapers.sh >/dev/null 2>&1
```
#### Auto-Login no LIGHTDM
```
$ sh -c 'echo "[SeatDefaults]\nautologin-user=$USER" >> 12-autologin.conf' && sudo mv 12-autologin.conf /etc/lightdm/lightdm.conf.d/12-autologin.conf
```

#### Ativar o Bluetooth.

```
$ sudo systemctl enable bluetooth
$ sudo systemctl restart bluetooth
$ systemctl status bluetooth
```

### Extenções VsCode

```
$ code --install-extension PKief.material-icon-theme
$ code --install-extension CoenraadS.bracket-pair-colorizer-2
$ code --install-extension naumovs.color-highlight
$ code --install-extension Nash.awesome-flutter-snippets
$ code --install-extension akamud.vscode-theme-onedark
```

# Release Upgrade

##### Escolher atualização (Caso queira só LTS ou Normal).
`$ sudo vim /etc/update-manager/release-upgrades`

##### Atualizar!
`$ sudo apt update && sudo apt upgrade -y && sudo do-release-upgrade`

##### Atualizar Drivers

`$ sudo ubuntu-drivers autoinstall`
